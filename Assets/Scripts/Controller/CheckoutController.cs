using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckoutController : Singleton<CheckoutController>
{
    [SerializeField] private GameObject boxPref;
    [SerializeField] private GameObject moneyPref;
    [SerializeField] private List<Transform> listPosClient;
    [SerializeField] private List<Transform> listPosMoney;
    [SerializeField] private Transform posBox;
    [HideInInspector] ClientController _client;

    [HideInInspector] public bool isPaying = false;
    [HideInInspector] public List<GameObject> listClientWaiting = new List<GameObject>();

    private Transform player;
    public List<MoneyController> listMoney = new List<MoneyController>();
    private Dictionary<Transform, bool> dicPosForClientWaiting = new Dictionary<Transform, bool>();
    private Dictionary<Transform, bool> dicPosForClientPay = new Dictionary<Transform, bool>();
    private Dictionary<Transform, int> dicMoney = new Dictionary<Transform, int>();
    private bool isHaveBox = false;
    private int totalMoneyOnCheckout = 0;

    GameObject _box;

    protected override void Awake()
    {
        base.Awake();
        dicPosForClientWaiting.Add(listPosClient[0], false);
        dicPosForClientWaiting.Add(listPosClient[1], false);
        dicPosForClientWaiting.Add(listPosClient[2], false);
        dicPosForClientWaiting.Add(listPosClient[3], false);

        dicPosForClientPay.Add(listPosClient[0], false);

        for(int i = 0; i < listPosMoney.Count; i++)
        {
            dicMoney.Add(listPosMoney[i], 0);
        }
    }

    private void Start()
    {
        InvokeRepeating(nameof(InitBox), 1, 1);
    }

    private void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    void InitBox()
    {
        if (isHaveBox || !dicPosForClientPay[listPosClient[0]]) return;
        
        if(PoolingController.Instance.listBox.Count > 0)
        {
            _box = PoolingController.Instance.listBox[0];
            PoolingController.Instance.listBox.Remove(_box);
        }
        else
        {
            _box = Instantiate(boxPref);
        }
        _box.transform.parent = transform;
        _box.transform.rotation = Quaternion.Euler(0, 0, 0);
        _box.transform.position = posBox.position;
        _box.transform.localScale = new Vector3(0.2f, 1, 1);
        _box.SetActive(true);
        isHaveBox = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            UtilsMain.isPlayerReceiveMoney = true;
            StartCoroutine(PlayerGetMoney());
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && dicPosForClientPay[listPosClient[0]] == true)
        {
            _client = GetClientIsPaying();
            if (_client != null && _box != null && !isPaying)
            {
                isPaying = true;
                StartCoroutine(PackingVegetable(_client.listVegetable));
            }
        } 
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            UtilsMain.isPlayerReceiveMoney = false;         
        }
    }

    IEnumerator PackingVegetable(List<Vegetable> _listVegetable)
    {
        if (_listVegetable.Count == 0) yield break;
        BoxController _boxController = _box.GetComponent<BoxController>();
        int n = _listVegetable.Count;
        for (int i = n - 1; i >= 0; i--)
        {
            _listVegetable[i].Move(_boxController.listPos[_boxController.listVegetable.Count], () =>
            {
                Vegetable _vegetable = _listVegetable[i];
                _vegetable.transform.parent = _box.transform;
                _vegetable.transform.position = _boxController.listPos[_boxController.listVegetable.Count].position;
                _vegetable.transform.localScale = new Vector3(0.3f, 0.6f, 0.3f);
                _boxController.listVegetable.Add(_vegetable);
                _listVegetable.Remove(_vegetable);
            });
            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(1f);
        //vegetable pack move to client
        _boxController.Move(_client.posBox, () =>
        {
            _boxController.transform.parent = _client.transform;
            _boxController.transform.position = _client.posBox.position;
            _boxController.transform.localScale = new Vector3(1, 0.5f, 1);
            _boxController.transform.rotation = Quaternion.Euler(0, 180, 0);
            
            //client pay money
            int price = _boxController.listVegetable[0].price;
            StartCoroutine(InitMoney(_boxController.listVegetable.Count, price));
        });
    }

    IEnumerator InitMoney(int count, int money)
    {
        if(listMoney.Count == listPosMoney.Count)
        {
            yield return new WaitForEndOfFrame();
            StartCoroutine(InitMoney(count, money));
        }
        else
        {
            int countVegetable = count;
            
            for (int i = 0; i < count; i++)
            {
                if (listMoney.Count == listPosMoney.Count) break;
                GameObject _money;
                if (PoolingController.Instance.listMoney.Count > 0)
                {
                    _money = PoolingController.Instance.listMoney[0];
                    PoolingController.Instance.listMoney.Remove(_money);
                }
                else
                {
                    _money = Instantiate(moneyPref);
                }

                _money.transform.parent = _client.transform;
                _money.transform.position = _client.transform.position;
                _money.transform.rotation = Quaternion.Euler(0, 0, 0);
                _money.transform.localScale = new Vector3(0.3f, 0.2f, 0.6f);
                _money.SetActive(true);
                _money.GetComponent<MoneyController>().Move(listPosMoney[listMoney.Count], 0.2f, () =>
                {
                    _money.transform.parent = transform;
                    _money.transform.position = listPosMoney[listMoney.Count].position;
                    _money.transform.rotation = Quaternion.Euler(0, 0, 0);
                    _money.transform.localScale = new Vector3(0.03f, 0.2f, 0.3f);
                    listMoney.Add(_money.GetComponent<MoneyController>());
                    dicMoney[listPosMoney[listMoney.Count - 1]] = money;
                });
                countVegetable--;
                yield return new WaitForSeconds(0.1f);
            }
            if(countVegetable > 0)
            {
                StartCoroutine(InitMoney(countVegetable, money));
            }
            else
            {
                _client.GetComponent<ClientMove>().PayCompleted();
                isHaveBox = false;
                _box = null;
                StartCoroutine(MoveClientIsWaiting());
            }
        }
    }

    IEnumerator MoveClientIsWaiting()
    {
        if (listClientWaiting.Count == 0) yield break;
        for(int i = 0; i < listClientWaiting.Count; i++)
        {
            listClientWaiting[i].GetComponent<ClientMove>().GoToEmptyPos();
            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator PlayerGetMoney()
    {
        if (listMoney.Count == 0 || !UtilsMain.isPlayerReceiveMoney)
        {
            yield return new WaitForEndOfFrame();
            StartCoroutine(PlayerGetMoney());
        }
        else
        {
            listMoney[listMoney.Count - 1].Move(player.transform, 0.2f, () =>
            {
                MoneyReachPlayer();
            });
            yield return new WaitForSeconds(0.1f);
            StartCoroutine(PlayerGetMoney());
        }
    }

    public void MoneyReachPlayer()
    {
        MoneyController _money = listMoney[listMoney.Count - 1].GetComponent<MoneyController>();
        UtilsMain.TotalMoney += dicMoney[listPosMoney[listMoney.Count - 1]];
        dicMoney[listPosMoney[listMoney.Count - 1]] = 0;
        listMoney.Remove(_money);
        PoolingController.Instance.listMoney.Add(_money.gameObject);
        _money.gameObject.SetActive(false);
    }

    ClientController GetClientIsPaying()
    {
        GameObject[] listClient = GameObject.FindGameObjectsWithTag("Client");
        for (int i = 0; i < listClient.Length; i++)
        {
            if (listClient[i].GetComponent<ClientController>().IsClientIsPaying())
                return listClient[i].GetComponent<ClientController>();
        }
        return null;
    }

    public Transform GetEmptyPos()
    {
        foreach (KeyValuePair<Transform, bool> pair in dicPosForClientWaiting)
        {
            if (!pair.Value)
            {
                return pair.Key;
            }
        }
        return null;
    }

    public Transform GetTransPosCheckout()
    {
        return listPosClient[0];
    }

    //set value for the waiting positions 
    public void SetValueForDicPosClient(Transform trans, bool value)
    {
        dicPosForClientWaiting[trans] = value;
    }

    //set value for the checkout position
    public void SetValueForPosCheckout(bool value)
    {
        dicPosForClientPay[listPosClient[0]] = value;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }
}
