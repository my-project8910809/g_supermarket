using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelvesController : MonoBehaviour
{
    [SerializeField] private TypeVegetable typeVegetable;
    [SerializeField] private List<Transform> listPosVegetable;
    [SerializeField] private List<Transform> listPosClient;

    [HideInInspector] public List<Vegetable> listVegetable;
    [HideInInspector] public List<GameObject> listClientWaiting;

    private Dictionary<Transform, bool> dicPosClient = new Dictionary<Transform, bool>();

    private int maxOfList => listPosVegetable.Count;
    Coroutine _importGoods;


    private void Awake()
    {
        dicPosClient.Add(listPosClient[0], false);
        dicPosClient.Add(listPosClient[1], false);
        dicPosClient.Add(listPosClient[2], false);
        dicPosClient.Add(listPosClient[3], false);
    }

    private void Start()
    {
        StartCoroutine(DeliveryVegetable());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _importGoods = StartCoroutine(ImportVegetable(PlayerController.Instance.listVegetable));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if(_importGoods != null)
                StopCoroutine(_importGoods);
        }
    }

    IEnumerator ImportVegetable(List<Vegetable> _listVegetable)
    {
        if (_listVegetable.Count == 0 || this.listVegetable.Count == maxOfList) yield break;
        int n = _listVegetable.Count;
        for(int i = n -1; i >= 0; i--)
        {
            if (this.listVegetable.Count == maxOfList) yield break;
            if (_listVegetable[i].typeVegetable != typeVegetable) continue;
            _listVegetable[i].Move(listPosVegetable[this.listVegetable.Count], () =>
            {
                Vegetable _vegetable = _listVegetable[i];
                
                _vegetable.transform.parent = transform.parent;
                _vegetable.transform.position = listPosVegetable[this.listVegetable.Count].position;
                _vegetable.transform.localScale = Vector3.one * 0.1f;
                this.listVegetable.Add(_vegetable);
                _listVegetable.Remove(_vegetable);
            });
            yield return new WaitForSeconds(0.1f);
        }
    }

    

    IEnumerator DeliveryVegetable()
    {
        if(listVegetable.Count == 0 || listClientWaiting.Count == 0)
        {
            yield return new WaitForEndOfFrame();
            StartCoroutine(DeliveryVegetable());
        }
        else
        {
            int randomClient = Random.Range(0, listClientWaiting.Count);
            ClientController _client = listClientWaiting[randomClient].GetComponent<ClientController>();
            int n = listVegetable.Count;
            for(int i = n - 1; i >= 0; i--)
            {
                if (_client.IsGetEnoughVegetable()) break;
                listVegetable[i].Move(_client.listTrans[_client.listVegetable.Count], () =>
                {
                    Vegetable _vegetable = listVegetable[i];
                    _vegetable.transform.parent = _client.transform;
                    _vegetable.transform.rotation = Quaternion.Euler(0, 0, 0);
                    _vegetable.transform.position = _client.listTrans[_client.listVegetable.Count].position;
                    _vegetable.transform.localScale = Vector3.one * 0.6f;
                    _client.listVegetable.Add(_vegetable);
                    _client.UpdateText();
                    listVegetable.Remove(_vegetable);
                });
                yield return new WaitForSeconds(0.1f);
            }
            if (_client.IsGetEnoughVegetable())
            {
                listClientWaiting.Remove(_client.gameObject);
                _client.GetComponent<ClientMove>().GetEnoughVegetable();
            }
            StartCoroutine(DeliveryVegetable());
        }
    }

    public Transform GetEmptyPos()
    {
        foreach (KeyValuePair<Transform, bool> pair in dicPosClient)
        {
            if (!pair.Value)
            {
                return pair.Key;
            }
        }
        return null;
    }

    public void SetValueForDicPos(Transform trans, bool value)
    {
        dicPosClient[trans] = value;
    }
}
