using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{
    public List<Transform> listPos;
    [HideInInspector] public List<Vegetable> listVegetable = new List<Vegetable>();

    private void OnEnable()
    {
        listVegetable.Clear();
    }

    public void Move(Transform targetPosition, Action moveCompleted)
    {
        transform.DOMove(targetPosition.position, 0.1f).SetEase(Ease.InBack).OnComplete(() =>
        {
            moveCompleted?.Invoke();
        });
    }
}
