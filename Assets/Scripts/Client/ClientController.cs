using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ClientController : MonoBehaviour
{
    [SerializeField] private ClientMove clientMove;
    [SerializeField] private int countVegetableNeedBuy;
    [SerializeField] private TextMeshProUGUI text;

    public List<Transform> listTrans;
    [HideInInspector] public TypeVegetable typeVegetableNeedBy;
    [HideInInspector] public List<Vegetable> listVegetable = new List<Vegetable>();
    public Transform posBox;

    
    bool isClientIsPaying = false;



    private void OnEnable()
    {
        if (countVegetableNeedBuy > listTrans.Count) countVegetableNeedBuy = listTrans.Count;
    }

    public void RandomVegetableNeedBuyAndAmout()
    {
        int ranType = UnityEngine.Random.Range(0, 1);
        typeVegetableNeedBy = (TypeVegetable)ranType;

        int ranAmout = UnityEngine.Random.Range(1, 5);
        countVegetableNeedBuy = ranAmout;

        UpdateText();
    }

    public void SetClientIsPaying(bool value)
    {
        isClientIsPaying = value;
    }
    
    public void UpdateText()
    {
        if(listVegetable.Count == countVegetableNeedBuy)
        {
            text.text = "Can Pay";
        }
        else
        {
            text.text = listVegetable.Count + "/" + countVegetableNeedBuy;
        }
    }

    public bool IsClientIsPaying()
    {
        return isClientIsPaying;
    }

    public bool IsGetEnoughVegetable()
    {
        return listVegetable.Count == countVegetableNeedBuy;
    }
}
