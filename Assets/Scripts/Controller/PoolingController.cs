using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingController : Singleton<PoolingController>
{
    public List<Vegetable> listVegetable = new List<Vegetable>();
    public List<GameObject> listBox = new List<GameObject>();
    public List<GameObject> listClient = new List<GameObject>();
    public List<GameObject> listMoney = new List<GameObject>();
    public List<GameObject> listResource = new List<GameObject>();
}
