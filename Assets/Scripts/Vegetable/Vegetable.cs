using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Vegetable : MonoBehaviour
{
    [HideInInspector] public TypeVegetable typeVegetable;
    public int price;


    public void Move(Transform targetPosition, Action moveCompleted)
    {
        transform.DOMove(targetPosition.position, 0.1f).SetEase(Ease.InBack).OnComplete(() =>
        {
            moveCompleted?.Invoke();
        });
    }
}
