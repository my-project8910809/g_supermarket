using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TotalMoney : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI coinTotal;

    private void Awake()
    {
        ActionController.totalMoneyChange += UpdateMoneyText;
        UpdateMoneyText();
    }
    

    public void UpdateMoneyText()
    {
        if (UtilsMain.TotalMoney < 1000)
        {
            coinTotal.text = UtilsMain.TotalMoney.ToString();
        }
        else
        {
            coinTotal.text = ((float)UtilsMain.TotalMoney / 1000).ToString("F1") + "K";
        }
    }


    private void OnDestroy()
    {
        ActionController.totalMoneyChange -= UpdateMoneyText;
    }
}
