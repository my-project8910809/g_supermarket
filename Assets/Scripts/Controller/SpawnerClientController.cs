using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerClientController : Singleton<SpawnerClientController>
{
    [HideInInspector] public List<GameObject> listClient = new List<GameObject>();
    [SerializeField] private GameObject clientPref;

    private int maxClient = 4;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(InitClient());
    }


    IEnumerator InitClient()
    {
        if(listClient.Count == maxClient)
        {
            yield return new WaitForEndOfFrame();
            StartCoroutine(InitClient());
        }
        else
        {
            yield return new WaitForSeconds(3f);
            GameObject _client;
            if(PoolingController.Instance.listClient.Count > 0)
            {
                _client = PoolingController.Instance.listClient[0];
                PoolingController.Instance.listClient.Remove(_client);
            }
            else
            {
                _client = Instantiate(clientPref);
            }
            _client.transform.position = gameObject.transform.position;
            _client.transform.rotation = Quaternion.Euler(0, 0, 0);
            _client.SetActive(true);
            listClient.Add(_client);
            _client.GetComponent<ClientController>().RandomVegetableNeedBuyAndAmout();

            StartCoroutine(InitClient());
        }
    }
}
