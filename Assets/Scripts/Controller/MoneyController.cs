using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyController : MonoBehaviour
{
    public void Move(Transform targetPosition, float time, Action moveCompleted)
    {
        transform.DOMove(targetPosition.position, time).SetEase(Ease.InBack).OnComplete(() =>
        {
            moveCompleted?.Invoke();
        });
    }
}
