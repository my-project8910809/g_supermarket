using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UtilsMain 
{
    public static int[] listPriceVegetable = { 10 };
    public static bool isPlayerReceiveMoney = false;
    public static bool isPlayerSpendMoney = false;

    public static int TotalMoney
    {
        get => PlayerPrefs.GetInt(Constants.TOTAL_MONEY, 0);
        set
        {
            PlayerPrefs.SetInt(Constants.TOTAL_MONEY, value);
            ActionController.totalMoneyChange?.Invoke();
        }
    }
}
