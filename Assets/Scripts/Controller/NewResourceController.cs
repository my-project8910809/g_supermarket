using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NewResourceController : MonoBehaviour
{
    [SerializeField] private TypeResource typeResource;
    [SerializeField] private List<GameObject> listResourcePref;
    [SerializeField] private List<Transform> listTransResource;
    [SerializeField] private GameObject moneyPref;
    [SerializeField] private int price;
    [SerializeField] private TextMeshProUGUI textPrice;
    [SerializeField] private Transform posMoney;

    private int moneyCoefficient = 5;
    Coroutine _spendMoney;

    // Start is called before the first frame update
    void Start()
    {
        UpdateTextPrice();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdateTextPrice()
    {
        textPrice.text = price + "";
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            _spendMoney = StartCoroutine(SpendMoney(other.gameObject));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if(_spendMoney != null)
                 StopCoroutine(_spendMoney);
        }
    }

    IEnumerator SpendMoney(GameObject parent)
    {
        if(UtilsMain.TotalMoney <= moneyCoefficient)
        {
            yield break;
        }
        int n = price / moneyCoefficient;
        for(int i = n - 1; i >= 0; i--)
        {
            if (UtilsMain.TotalMoney < moneyCoefficient) yield break;
            GameObject _money;
            if(PoolingController.Instance.listMoney.Count > 0)
            {
                _money = PoolingController.Instance.listMoney[0];
                PoolingController.Instance.listMoney.Remove(_money);
            }
            else
            {
                _money = Instantiate(moneyPref);
            }
            _money.transform.parent = parent.transform;
            _money.transform.position = parent.transform.position;
            _money.transform.localScale = new Vector3(0.6f, 0.4f, 1.2f);
            _money.transform.rotation = Quaternion.Euler(0, 0, 0);
            _money.SetActive(true);
            _money.GetComponent<MoneyController>().Move(transform, 0.5f, () =>
            {
                UtilsMain.TotalMoney -= moneyCoefficient;
                price -= moneyCoefficient;
                UpdateTextPrice();
                PoolingController.Instance.listMoney.Add(_money);
                _money.SetActive(false);
            });
            yield return new WaitForSeconds(0.2f);
        }
        InitResource();
    }

    void InitResource()
    {
        GameObject resource = GetResourceByType(typeResource);
        switch (typeResource)
        {
            case TypeResource.Tree:
                resource.transform.localScale = Vector3.one * 4;
                break;
            case TypeResource.Shelves:
                resource.transform.localScale = Vector3.one * 10;
                break;
        }
        
        resource.transform.position = listTransResource[(int)typeResource].position;
        resource.transform.rotation = Quaternion.identity;

        gameObject.SetActive(false);
    }

    private GameObject GetResourceByType(TypeResource _typeResource)
    {
        if (PoolingController.Instance.listResource.Count > 0)
        {
            foreach (GameObject obj in PoolingController.Instance.listResource)
            {
                if (obj.TryGetComponent(out GameObject resource))
                {
                    if (resource.GetComponent<NewResourceController>().typeResource == _typeResource)
                    {
                        PoolingController.Instance.listResource.Remove(obj);
                        obj.gameObject.SetActive(true);
                        return obj;
                    }
                }
            }
        }
        else
        {
            return Instantiate(listResourcePref[(int)_typeResource]);
        }
        return null;
    }
}
