using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Singleton<PlayerController>
{
    [HideInInspector] public List<Vegetable> listVegetable = new List<Vegetable>();
    [SerializeField] private List<Transform> listTransVegetbale;
    [SerializeField] private GameObject textMax;

    private int maxOfList => listTransVegetbale.Count;

    Coroutine _getVegetable;

    
    private void Update()
    {
        CheckMaxVegetable();
    }

    void CheckMaxVegetable()
    {
        if (listVegetable.Count == maxOfList)
        {
            textMax.SetActive(true);
        }
        else
        {
            textMax.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PotatoTree")
        {
            _getVegetable = StartCoroutine(GetVegetable(other.GetComponent<VegetableTree>()));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PotatoTree")
        {
            if(_getVegetable != null)
            {
                StopCoroutine(_getVegetable);
                SetCorrectPosForVegetable();
            }
        }
    }

    IEnumerator GetVegetable(VegetableTree vegetableTree)
    {
        if (listVegetable.Count == maxOfList) yield break;
        if(vegetableTree.listVegetable.Count == 0)
        {
            yield return null;
            _getVegetable = StartCoroutine(GetVegetable(vegetableTree));
        }
        else
        {
            int n = vegetableTree.listVegetable.Count;
            for(int i = n - 1; i >= 0; i--)
            {
                if(listVegetable.Count == maxOfList)
                {
                    CheckMaxVegetable();
                    yield break;
                }
                vegetableTree.listVegetable[i].Move(listTransVegetbale[listVegetable.Count], () =>
                {
                    Vegetable _vegetable = vegetableTree.listVegetable[i];
                    
                    _vegetable.transform.parent = transform;
                    _vegetable.transform.position = listTransVegetbale[listVegetable.Count].position;
                    _vegetable.transform.rotation = Quaternion.Euler(0, 0, 0);
                    _vegetable.transform.localScale = Vector3.one * 0.6f;
                    listVegetable.Add(_vegetable);
                    vegetableTree.listVegetable.Remove(_vegetable);
                });
                yield return new WaitForSeconds(0.1f);
            }
            _getVegetable = StartCoroutine(GetVegetable(vegetableTree));
        }
    }

    void SetCorrectPosForVegetable()
    {
        for(int i = 0; i < listVegetable.Count; i++)
        {
            if(listVegetable[i].transform.position != listTransVegetbale[i].transform.position)
            {
                listVegetable[i].transform.position = listTransVegetbale[i].transform.position;
            }
        }
    }
}
