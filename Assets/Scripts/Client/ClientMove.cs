using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientMove : MonoBehaviour
{
    
    private GameObject[] listShelves;
    private float moveSpeed = 5f;
    private Transform shelvesPosition;

    [HideInInspector] public Transform checkoutPosition;
    [HideInInspector] public Transform goOutPosition;
    [HideInInspector] public CheckoutController _checkout;
    [HideInInspector] public ShelvesController _shelves;

    bool isReachShelves = false;
    bool isReachCheckout = false;
    bool isReceiveEnoughVegetable = false;
    bool isReachGoOutPos = false;
    bool isPayCompleted = false;

    private void OnEnable()
    {
        ResetVar();

        goOutPosition = GameObject.FindGameObjectWithTag("GoOutPos").transform;
        _checkout = GameObject.FindGameObjectWithTag("Checkout").GetComponent<CheckoutController>();
        

        StartCoroutine(GetTarget());
    }

    IEnumerator GetTarget()
    {
        yield return new WaitForSeconds(0.5f);
        listShelves = GameObject.FindGameObjectsWithTag(gameObject.GetComponent<ClientController>().typeVegetableNeedBy.ToString() + "Shop");
        shelvesPosition = GetShelvesGoTo().GetComponentInChildren<ShelvesController>().GetEmptyPos();
        _shelves = GetShelvesGoTo().GetComponentInChildren<ShelvesController>();
        _shelves.SetValueForDicPos(shelvesPosition, true);
    }

    GameObject GetShelvesGoTo()
    {
        if (listShelves.Length > 1)
        {
            //find the shelves with the most vegetable
            int maxVegetable = 0;
            int index = 0;
            for (int i = 0; i < listShelves.Length; i++)
            {
                if (maxVegetable < listShelves[i].GetComponentInChildren<ShelvesController>().listVegetable.Count)
                {
                    maxVegetable = listShelves[i].GetComponentInChildren<ShelvesController>().listVegetable.Count;
                    index = i;
                }
            }
            return listShelves[index];
        }
        else
        {
            return listShelves[0];
        }
    }

    public void GetEnoughVegetable()
    {
        checkoutPosition = _checkout.GetEmptyPos();
        _shelves.SetValueForDicPos(shelvesPosition, false);
        _checkout.SetValueForDicPosClient(checkoutPosition, true);
        _checkout.listClientWaiting.Add(gameObject);
        isReceiveEnoughVegetable = true;
    }

    public void GoToEmptyPos()
    {
        _checkout.SetValueForDicPosClient(checkoutPosition, false);
        checkoutPosition = _checkout.GetEmptyPos();
        _checkout.SetValueForDicPosClient(checkoutPosition, true);
        isReachCheckout = false;
    }

    public void PayCompleted()
    {
        isPayCompleted = true;
        _checkout.listClientWaiting.Remove(gameObject);
        _checkout.SetValueForPosCheckout(false);
        _checkout.SetValueForDicPosClient(checkoutPosition, false);
        gameObject.GetComponent<ClientController>().SetClientIsPaying(false);
        _checkout.isPaying = false;
    }

    void ResetVar()
    {
        isReachShelves = false;
        isReachCheckout = false;
        isReceiveEnoughVegetable = false;
        isPayCompleted = false;
        isReachGoOutPos = false;
    }

    private void Update()
    {
        if (shelvesPosition == null) return;
        //go to the shelves
        if (transform.position == shelvesPosition.position && !isReachShelves)
        {
            isReachShelves = true;
            _shelves.listClientWaiting.Add(gameObject);
        }
        else if (!isReachShelves)
        {
            transform.position = Vector3.MoveTowards(transform.position, shelvesPosition.position, moveSpeed * Time.deltaTime);
        }

        //go to checkout
        if (isReceiveEnoughVegetable)
        {
            if (transform.position == checkoutPosition.position && !isReachCheckout)
            {
                isReachCheckout = true;
               
                //the first position
                if (transform.position == _checkout.GetTransPosCheckout().position)
                {
                    _checkout.SetValueForPosCheckout(true);
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                    gameObject.GetComponent<ClientController>().SetClientIsPaying(true);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, -90, 0);
                }
            }
            else if (!isReachCheckout)
            {
                transform.position = Vector3.MoveTowards(transform.position, checkoutPosition.position, moveSpeed * Time.deltaTime);
            }
        }

        //go out
        if (isPayCompleted && !isReachGoOutPos)
        {
            if(transform.position == goOutPosition.position && !isReachGoOutPos)
            {
                isReachGoOutPos = true;
                HandleAddPooling();
            }
            else if (!isReachGoOutPos)
            {
                transform.rotation = Quaternion.Euler(0, -90, 0);
                transform.position = Vector3.MoveTowards(transform.position, goOutPosition.position, moveSpeed * Time.deltaTime);
            }
        }
    }

    void HandleAddPooling()
    {
        Vegetable[] _listVegetable = GetComponentsInChildren<Vegetable>();
        BoxController _boxController = GetComponentInChildren<BoxController>();
        PoolingController.Instance.listBox.Add(_boxController.gameObject);
        _boxController.gameObject.SetActive(false);
        for(int i = 0; i < _listVegetable.Length; i++)
        {
            PoolingController.Instance.listVegetable.Add(_listVegetable[i]);
            _listVegetable[i].gameObject.SetActive(false);
        }
        PoolingController.Instance.listClient.Add(gameObject);
        SpawnerClientController.Instance.listClient.Remove(gameObject);
        gameObject.SetActive(false);
    } 
}
