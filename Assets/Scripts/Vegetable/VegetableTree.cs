using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetableTree : MonoBehaviour
{
    [SerializeField] private TypeVegetable typeVegetable;
    [SerializeField] private Vegetable vegetablePref;
    [SerializeField] private List<Transform> listTrans;
    [SerializeField] private float timeInitOneVegetable = 1f;
    [HideInInspector] public List<Vegetable> listVegetable = new List<Vegetable>();

    private int countMax => listTrans.Count;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(InitVegetable());
    }

    IEnumerator InitVegetable()
    {
        if (listVegetable.Count == countMax)
        {
            yield return null;
            StartCoroutine(InitVegetable());
        }
        else
        {
            yield return new WaitForSeconds(timeInitOneVegetable);
            Vegetable _vegetable = GetVegetableByType();
            _vegetable.transform.parent = transform;
            _vegetable.transform.localScale = Vector3.one * 0.2f;
            _vegetable.transform.position = listTrans[listVegetable.Count].position;
            _vegetable.typeVegetable = typeVegetable;
            _vegetable.price = UtilsMain.listPriceVegetable[(int)typeVegetable];
            listVegetable.Add(_vegetable);
            StartCoroutine(InitVegetable());
        }
    }

    private Vegetable GetVegetableByType()
    {
        if(PoolingController.Instance.listVegetable.Count > 0)
        {
            foreach (Vegetable obj in PoolingController.Instance.listVegetable)
            {
                if (obj.TryGetComponent(out Vegetable vegetable))
                {
                    if (vegetable.typeVegetable == typeVegetable)
                    {
                        PoolingController.Instance.listVegetable.Remove(obj);
                        obj.gameObject.SetActive(true);
                        return obj;
                    }
                }
            }
        }
        else
        {
            return Instantiate(vegetablePref);
        }
        return null;
    }
}
